package um;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class Brasileirao extends Campeonato {

    public Brasileirao() {
        super("Brasileirão");
    }
    
    public static void main(String[] args) {
        Brasileirao brasileirao = new Brasileirao();
        Clube palmeiras = new Clube("Palmeiras");
        Clube santos = new Clube("Santos");
        Clube corinthians = new Clube("Corinthians");
        brasileirao.adicionarClubeParticipante(palmeiras);
        brasileirao.adicionarClubeParticipante(santos);
        brasileirao.adicionarClubeParticipante(corinthians);
        Partida partida1 = brasileirao.adicionarPartida(santos, palmeiras, 5, 1);
        brasileirao.addPartida(partida1);
        Partida partida2 = brasileirao.adicionarPartida(corinthians, palmeiras, 0, 0);
        brasileirao.addPartida(partida2);
        Partida partida3 = brasileirao.adicionarPartida(corinthians, santos, 0, 3);
        brasileirao.addPartida(partida3);
        Partida partida4 = brasileirao.adicionarPartida(palmeiras, corinthians, 1, 0);
        brasileirao.addPartida(partida4);
        System.out.println(santos.toString());
        System.out.println(palmeiras.toString());
        System.out.println(corinthians.toString());
        List<Clube> listaClubesOrdenada = brasileirao.getClubesParticipantes();
        Collections.sort(listaClubesOrdenada, Collections.reverseOrder());
        for (Iterator iterator = listaClubesOrdenada.iterator(); iterator.hasNext();) {
            Clube clube = (Clube) iterator.next();
            System.out.println(clube.getNome());
			
		}
    }

    public Partida adicionarPartida(Clube timeDaCasa, Clube visitante, int golsCasa, int golsVisitante) {
        Partida partida = new Partida(timeDaCasa, visitante);
        partida.setGolsCasa(golsCasa);
        partida.setGolsVisitante(golsVisitante);
        timeDaCasa.adicionarGolsContra(golsVisitante);
        timeDaCasa.adicionarGolsPro(golsCasa);
        visitante.adicionarGolsContra(golsCasa);
        visitante.adicionarGolsPro(golsVisitante);
        if (partida.getVitorioso()==null) {
            System.out.println("Partida terminou empatada.");
            timeDaCasa.adicionarPontos(1);
            visitante.adicionarPontos(1);
        } else {
            partida.getVitorioso().adicionarPontos(3);
            System.out.println("O time vencedor foi " + partida.getVitorioso().getNome());
        }
        System.out.println(timeDaCasa.getNome() + " " + partida.getGolsCasa() + " X " + partida.getGolsVisitante() + " " + visitante);
        return partida; 
    }

}