package um;

public class Partida {
    
    private Clube timeDaCasa;
    private Clube visitante;
    private int golsCasa;
    private int golsVisitante;

    public Partida(Clube timeDaCasa, Clube visitante) {
        this.timeDaCasa = timeDaCasa;
        this.visitante = visitante;
    }

    public void setTimeDaCasa(Clube timeDaCasa) {
        this.timeDaCasa = timeDaCasa;
    }

    public Clube getTimeDaCasa() {
        return timeDaCasa;
    }

    public void setVisitante(Clube visitante) {
        this.visitante = visitante;
    }

    public Clube getVisitante() {
        return visitante;
    }

    public void setGolsCasa(int golsCasa) {
        this.golsCasa = golsCasa;
    }

    public int getGolsCasa() {
        return golsCasa;
    }

    public void setGolsVisitante(int golsVisitante) {
        this.golsVisitante = golsVisitante;
    }

    public int getGolsVisitante() {
        return golsVisitante;
    }

    public Clube getVitorioso() {
        if (golsCasa > golsVisitante) {
            return timeDaCasa;
        } else if (golsCasa < golsVisitante) {
            return visitante;
        }
        return null;    // empate
    }

}
