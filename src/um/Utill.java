package um;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Utill {
	
	public static int capturaValor(String pergunta) {
		int retorno = -1;
		BufferedReader buf = new BufferedReader(new InputStreamReader(System.in));   
		while (retorno <= -1) {
			System.out.println(pergunta);
			try { 
				retorno = Integer.valueOf(buf.readLine());		
			} catch (Exception e) {
				System.out.println("Erro ao capturar número!");
				e.printStackTrace();
			}
		}
		return retorno;
	}

}
