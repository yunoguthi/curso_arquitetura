package um;

import java.util.Random;

public class E5 {
	
	public static void main(String[] args) {
		int media = 0;
		int[] notas = new int[3];
		Random random = new Random();
		for (int i = 0; i < 3; i++) {
			notas[i] = random.nextInt(10);
			System.out.print(notas[i]);
			if (i!=2) {
				System.out.print(";");
			}
			media += notas[i];
		}
		System.out.println(" - Média final: " + media/3);
		
	}

}
