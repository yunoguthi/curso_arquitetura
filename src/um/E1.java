package um;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class E1 {

	public static void main(String[] args) {
		ArrayList<Integer> lista = new ArrayList<Integer>();
		BufferedReader buf = new BufferedReader(new InputStreamReader(System.in));   
		int cont = 0;
		while (cont < 3) {
			System.out.println("Digite 3 números (" + ++cont + "/3)");
			try { 
				lista.add(Integer.valueOf(buf.readLine()));		
			} catch (Exception e) {
				System.out.println("Erro ao capturar número!");
				e.printStackTrace();
				cont--;
			}
		}
		Collections.sort(lista);
		for (Iterator iterator = lista.iterator(); iterator.hasNext();) {
			Integer integer = (Integer) iterator.next();
			System.out.println(integer);
		}
	}

}
