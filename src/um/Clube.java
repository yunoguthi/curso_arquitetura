package um;

import java.util.Comparator;

public class Clube implements Comparable<Clube>{

    private String nome;
    private int pontos;
    private int golsPro;
    private int golsContra;

    public Clube(String nome) {
        this.nome = nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }

    public void setPontos(int pontos) {
        this.pontos = pontos;
    }

    public int getPontos() {
        return pontos;
    }

    public void adicionarPontos(int pontos) {
        this.pontos += pontos;
    }

    public void setGolsPro(int golsPro) {
        this.golsPro = golsPro;
    }

    public int getGolsPro() {
        return golsPro;
    }

    public void adicionarGolsPro(int golsPro) {
        this.golsPro += golsPro;
    }

    public void setGolsContra(int golsContra) {
        this.golsContra = golsContra;
    }

    public int getGolsContra() {
        return golsContra;
    }

    public void adicionarGolsContra(int golsContra) {
        this.golsContra += golsContra;
    }

    @Override
    public String toString() {
        return nome + ": " + pontos + " pontos, " + golsPro + " gols pro, " + golsContra + " gols contra";
    }

    @Override
    public int compareTo(Clube c) {
        return Integer.compare(this.getPontos(), c.getPontos());
    }
    
}
