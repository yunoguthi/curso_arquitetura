package um;

import java.util.ArrayList;
import java.util.List;

public class Campeonato {
    
    private List<Clube> clubesParticipantes = new ArrayList<Clube>();
    private List<Partida> partidas = new ArrayList<Partida>();
    private String nome = "";

    public Campeonato(String nome) {
        this.nome = nome;
    }

    public void addPartida(Partida partida){
        partidas.add(partida);
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }

    public List<Partida> getPartidas() {
        return partidas;
    }

    public List<Clube> getClubesParticipantes() {
        return clubesParticipantes;
    }

    public void adicionarClubeParticipante(Clube clube) {
        this.clubesParticipantes.add(clube);
    }
        

}
